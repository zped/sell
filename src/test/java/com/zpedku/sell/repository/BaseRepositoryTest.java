package com.zpedku.sell.repository;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yalith
 * @Time 2018-2018/6/27-1:07
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseRepositoryTest {
}
