package com.zpedku.sell.repository;

import com.zpedku.sell.entity.OrderMaster;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

public class OrderMasterRepositoryTest extends BaseRepositoryTest {
    @Autowired
    private OrderMasterRepository orderMasterRepository;

    private final String OPENID = "123456";

    @Test
    public void saveTest() {
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setOrderId("123457");
        orderMaster.setBuyerName("zpedku");
        orderMaster.setBuyerAddress("foshan");
        orderMaster.setBuyerPhone("13750024364");
        orderMaster.setBuyerOpenid("123456");
        orderMaster.setOrderAmount(new BigDecimal(99.2));
        orderMaster.setCreateTime(new Date());
        orderMasterRepository.save(orderMaster);
    }


    @Test
    public void findByBuyerOpenid() throws Exception {
        Pageable pageRequest = new PageRequest(0,10);
        Page<OrderMaster> page = orderMasterRepository.findByBuyerOpenid(OPENID, pageRequest);
        System.out.println(page.getTotalElements());
    }

}