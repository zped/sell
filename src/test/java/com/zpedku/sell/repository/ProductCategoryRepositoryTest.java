package com.zpedku.sell.repository;

import com.zpedku.sell.entity.ProductCategory;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductCategoryRepositoryTest {

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Test
    public void findByCategoryTypeIn() throws Exception {
        List<Integer> types = Arrays.asList(1,2,3,4);
        List<ProductCategory> result= productCategoryRepository.findByCategoryTypeIn(types);
        Assert.assertNotEquals(null,result);

    }


    @Test
    public void findOne() throws Exception {
        ProductCategory category = productCategoryRepository.findOne(1);
        log.info("category: {}",category);

    }

    @Test
    public void save() {
        ProductCategory category = new ProductCategory("男生最爱",3);
        productCategoryRepository.save(category);
    }
}