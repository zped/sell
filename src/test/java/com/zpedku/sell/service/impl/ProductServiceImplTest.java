package com.zpedku.sell.service.impl;

import com.zpedku.sell.SellApplicationTests;
import com.zpedku.sell.entity.ProductInfo;
import com.zpedku.sell.enums.ProductStatusEnum;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class ProductServiceImplTest extends SellApplicationTests {
   @Autowired
   private ProductServiceImpl productServiceImpl;
    @Test
    public void findOne() throws Exception {
        ProductInfo one = productServiceImpl.findOne("123456");
        Assert.assertNotNull(one);
    }

    @Test
    public void findUpAll() throws Exception {

        List<ProductInfo> upAll = productServiceImpl.findUpAll();

    }

    @Test
    public void findAll() throws Exception {
        PageRequest pageRequest = new PageRequest(0,2);
        Page<ProductInfo> pages = productServiceImpl.findAll(pageRequest);
        System.out.println(pages.getTotalElements());
    }

    @Test
    public void save() throws Exception {
       ProductInfo productInfo =new ProductInfo();
        productInfo.setProductId("123");
        productInfo.setProductName("西红柿");
        productInfo.setProductPrice(new BigDecimal(3.2));
        productInfo.setProductStock(100);
        productInfo.setProductDescription("11111");
        productInfo.setProductIcon("http://www.baidu.com");
        productInfo.setProductStatus(ProductStatusEnum.DOWN.getCode());
        productInfo.setCategoryType(2);
        productServiceImpl.save(productInfo);
    }

}