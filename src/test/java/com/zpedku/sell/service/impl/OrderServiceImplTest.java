package com.zpedku.sell.service.impl;

import com.zpedku.sell.dto.OrderDTO;
import com.zpedku.sell.entity.OrderDetail;
import com.zpedku.sell.enums.OrderStatusEnum;
import com.zpedku.sell.repository.BaseRepositoryTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class OrderServiceImplTest extends BaseRepositoryTest {
    @Autowired
    private OrderServiceImpl orderServiceImpl;

    private final String BUYER_OPENID = "123123";

    private final String ORDER_ID = "1530521472790476067";

    @Test
    public void create() throws Exception {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName("wu");
        orderDTO.setBuyerAddress("茂名");
        orderDTO.setBuyerPhone("13750024364");
        orderDTO.setBuyerOpenid(BUYER_OPENID);

        //购物车
        List<OrderDetail> orderDetailList = new ArrayList<>();
        OrderDetail o1 = new OrderDetail();
        o1.setProductId("123");
        o1.setProductQuantity(1);
        orderDetailList.add(o1);

        orderDTO.setOrderDetailList(orderDetailList);

        OrderDTO result = orderServiceImpl.create(orderDTO);
        log.info("创建订单完成 result={}",result);
    }

    @Test
    public void findOne() throws Exception {
        OrderDTO orderDTO = orderServiceImpl.findOne(ORDER_ID);
        log.info("【订单详情】orderDTO={}",orderDTO);
    }

    @Test
    public void findList() throws Exception {
        PageRequest page = new PageRequest(0,2);
        Page<OrderDTO> orderDTOS = orderServiceImpl.findList(BUYER_OPENID, page);
//        log.info("【orderDTOS】{}",orderDTOS);
        log.info("【orderDTOS】{}",orderDTOS.getContent());


    }

    @Test
    public void cancel() throws Exception {
        OrderDTO orderDTO = orderServiceImpl.findOne(ORDER_ID);
        OrderDTO result = orderServiceImpl.cancel(orderDTO);
        Assert.assertEquals(OrderStatusEnum.CANCEL.getCode(),result.getOrderStatus());

    }

    @Test
    public void finish() throws Exception {
    }

    @Test
    public void paid() throws Exception {
    }

}