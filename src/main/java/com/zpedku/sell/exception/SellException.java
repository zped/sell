package com.zpedku.sell.exception;

import com.zpedku.sell.enums.ResultEnum;
import lombok.Data;
import lombok.Getter;

/**
 * @author yalith
 * @Time 2018-2018/7/2-15:42
 */
@Getter
public class SellException extends RuntimeException {
    private Integer code;

    public SellException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public SellException(Integer code,String message) {
        super(message);
        this.code = code;
    }
}
