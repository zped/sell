package com.zpedku.sell.converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zpedku.sell.dto.OrderDTO;
import com.zpedku.sell.entity.OrderDetail;
import com.zpedku.sell.enums.ResultEnum;
import com.zpedku.sell.exception.SellException;
import com.zpedku.sell.form.OrderForm;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author yalith
 * @Time 2018-2018/7/3-2:45
 */
@Slf4j
public class OrderForm2OrderDTOConverter {
    public static OrderDTO convert(OrderForm orderForm) {
        Gson gson = new Gson();
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerOpenid(orderForm.getOpenid());
        orderDTO.setBuyerName(orderForm.getName());
        orderDTO.setBuyerAddress(orderForm.getAddress());
        orderDTO.setBuyerPhone(orderForm.getPhone());
        List<OrderDetail> orderDetailList = null;
        try {
            orderDetailList = gson.fromJson(orderForm.getItems(), new
                    TypeToken<List<OrderDetail>>() {
                    }.getType());

        } catch (Exception e) {
            log.error("【对象转换】错误,string={}", orderForm.getItems());
            throw new SellException(ResultEnum.PARAM_ERROR);
        }
        orderDTO.setOrderDetailList(orderDetailList);

        return orderDTO;
    }


}
