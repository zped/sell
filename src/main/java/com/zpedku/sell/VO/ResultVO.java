package com.zpedku.sell.VO;

import lombok.Data;

/**
 * @author yalith
 * @Time 2018-2018/6/23-0:00
 */
@Data
public class ResultVO<T> {
    /*错误码*/
    private Integer code;

    /*提示信息*/
    private String msg;

    /*返回内容*/
    private T data;

}
