package com.zpedku.sell.dto;

import lombok.Data;

/**
 * 购物车
 *
 * @author yalith
 * @Time 2018-2018/7/2-16:04
 */
@Data
public class CartDTO {
    private String productId;

    private Integer productQuantity;

    public CartDTO() {
    }

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
