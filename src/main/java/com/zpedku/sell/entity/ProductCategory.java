package com.zpedku.sell.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author yalith
 * @Time 2018-2018/6/20-17:55
 */
@Entity
@DynamicUpdate//动态更新
@Data
public class ProductCategory {

    @Id
    @GeneratedValue
    private Integer categoryId;

    /*类目名字*/
    private String categoryName;
    /*类目编号*/
    private Integer categoryType;

    private Date creatTime;

    private Date updateDate;

    public ProductCategory() {

    }

    public ProductCategory(String categoryName, Integer categoryType) {
        this.categoryName = categoryName;
        this.categoryType = categoryType;
    }
}
