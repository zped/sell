package com.zpedku.sell.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * @author yalith
 * @Time 2018-2018/6/22-22:55
 */
@Entity
@Data
public class ProductInfo {
    @Id
    private String productId;

    /*名字*/
    private String productName;

    /*价格*/
    private BigDecimal productPrice;

    /*库存*/
    private Integer productStock;

    /*描述*/
    private String productDescription;

    /*小图链接地址*/
    private String productIcon;

    /*状态,0正常1下架*/
    private Integer productStatus;

    private Integer categoryType;

}
