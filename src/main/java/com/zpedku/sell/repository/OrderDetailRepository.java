package com.zpedku.sell.repository;

import com.zpedku.sell.dto.OrderDTO;
import com.zpedku.sell.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, String> {


    List<OrderDetail> findByOrderId(String orderId);
}
