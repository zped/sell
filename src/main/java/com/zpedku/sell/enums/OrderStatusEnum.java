package com.zpedku.sell.enums;

import lombok.Data;
import lombok.Getter;

/**
 * @author yalith
 * @Time 2018-2018/6/27-0:31
 */
@Getter
public enum OrderStatusEnum {
    NEW(0, "新订单"),

    FINISHED(1, "完结"),

    CANCEL(2, "已取消"),;


    private Integer code;

    private String message;

    OrderStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
