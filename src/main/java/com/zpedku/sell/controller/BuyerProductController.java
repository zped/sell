package com.zpedku.sell.controller;

import com.zpedku.sell.VO.ProductInfoVO;
import com.zpedku.sell.VO.ProductVO;
import com.zpedku.sell.VO.ResultVO;
import com.zpedku.sell.entity.ProductCategory;
import com.zpedku.sell.entity.ProductInfo;
import com.zpedku.sell.service.CategoryService;
import com.zpedku.sell.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yalith
 * @Time 2018-2018/6/22-23:58
 */
@RestController
@RequestMapping("/buyer/product")
public class BuyerProductController {
    @Autowired
    private ProductService productService;

    private CategoryService categoryService;

    @GetMapping("/list")
    public ResultVO list() {
        ResultVO result = new ResultVO();

        //1.查询所有上架商品
        List<ProductInfo> productInfoList = productService.findUpAll();

        //2.查询类目
        //和迭代一样,获取类目
        List<Integer> categoryTypeList = productInfoList .stream().map(e -> {
            return e.getCategoryType();
        }).collect(Collectors.toList());

        List<ProductCategory> productCategoryList = categoryService.findByCategoryTypeIn(categoryTypeList);

        //3.数据拼接
        List<ProductVO> productVOList =new ArrayList<>();

        for (ProductCategory productCategory : productCategoryList) {
            ProductVO productVO = new ProductVO();
            productVO.setCategoryName(productCategory.getCategoryName());
            productVO.setCategoryType(productCategory.getCategoryType());
            List<ProductInfoVO> productInfoVOList = new ArrayList<>();
            for (ProductInfo productInfo : productInfoList) {
                ProductInfoVO productInfoVO = new ProductInfoVO();
                BeanUtils.copyProperties(productInfo,productInfoVO);
                productInfoVOList.add(productInfoVO);
            }
            productVO.setProductInfoVOList(productInfoVOList);
            productVOList.add(productVO);
        }

        return result;
    }

}
