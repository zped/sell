package com.zpedku.sell.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author yalith
 * @Time 2018-2018/7/4-11:17
 */
@Data
@Component
@ConfigurationProperties(prefix = "wechat")
public class WechatConfig {

    private String appID;

    private String appsecret;

    private String token;


}


